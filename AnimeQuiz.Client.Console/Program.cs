﻿using AnimeQuiz.Client.Lib;
using AnimeQuiz.Lib;
using AnimeQuiz.Lib.Messages;
using AnimeQuiz.Lib.Messages.Requests;
using AnimeQuiz.Lib.Messages.Responses;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AnimeQuiz.Client.Console
{
    public static class Program
    {
        private static AnimeQuizClient client = null;

        static void Main(string[] args)
        {
            IPEndPoint serverEndPoint;

            bool noException = false;

            do
            {
                try
                {
                    System.Console.WriteLine("Test Client: Vers. 01.00");
                    System.Console.WriteLine("------------------------\n");

                    System.Console.WriteLine("Server Ip:");
                    System.Console.Write(">>");

                    IPAddress ipAddress = IPAddress.Parse(System.Console.ReadLine());

                    System.Console.WriteLine("Server Port:");
                    System.Console.Write(">>");

                    int port = Convert.ToInt32(System.Console.ReadLine());

                    client = new AnimeQuizClient(new IPEndPoint(ipAddress, port));
                    client.ConnectToServer();
                    client.MessageReceived += DisplayMessageReceived;
                    client.InvalidMessageReceived += InvalidMessageReceivedEventHandler;
                    client.Disconnected += Disconnected;

                    Task.Run(() => client.StartRunning());

                    ConnectionMessageRequest connectionRequest = new ConnectionMessageRequest();

                    client.SendMessageToQueue(connectionRequest, ConnectionResponseEventHandler);

                    while (true)
                    {
                        if (client.ClientID != Guid.Empty)
                        {
                            PingMessageRequest pingRequest = new PingMessageRequest(client.ClientID, client.ServerID, DateTime.UtcNow);


                            client.SendMessageToQueue(pingRequest, PingResponseEventHandler);
							Thread.Sleep(5000);
                        }
                    }
                }
                catch (Exception ex)
                {
                    AnimeQuizLogger.WriteExceptionToLog(ex, "C:\\ProgrammTestLogs\\ClientLog.txt");
                }
            } while (!noException);
        }

        public static void PingResponseEventHandler(MessageBase message)
        {
            PingMessageResponse ping = message as PingMessageResponse;

            if (ping != null)
            {
                DateTime currentTime = DateTime.UtcNow;

                TimeSpan diffPingTime = currentTime.Subtract(ping.PingTime);

                System.Console.WriteLine($"--> Ping received: {diffPingTime.Milliseconds}");
            }
        }

        public static void ConnectionResponseEventHandler(MessageBase message)
        {
            ConnectionMessageResponse connect = message as ConnectionMessageResponse;

            if (connect != null)
            {
                if (connect.Sender != Guid.Empty)
                    client.ServerID = connect.Sender;
                else
                    throw new ArgumentException("Guid is invalid");
            }
        }

        public static void DisplayMessageReceived(MessageBase message)
        {
            if (message != null)
            {
                System.Console.WriteLine($"{DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.fff")} --> Message Received, of type: {message.GetType().ToString()}");
                System.Console.WriteLine($"\tfrom <{message.Sender}>\n");
            }
            else
            {
                System.Console.WriteLine($"{DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.fff")} --> Unreadable Message Received");
            }
        }

        public static void InvalidMessageReceivedEventHandler(Guid senderID)
        {
            if (senderID == client.ServerID)
            {
                System.Console.WriteLine($"{DateTime.Now.ToString()}-->Invalid Message received from the Server.");
            }
            else
            {
                System.Console.WriteLine($"{DateTime.Now.ToString()}-->Unknown Invalid Message Received.");
            }
        }

        public static void Disconnected(Guid serverID)
        {
            System.Console.WriteLine("Disconnected");
            if (client.ServerID == serverID)
            {
                client = null;
            }
        }

    }
}
