﻿/* 
 * Erstelldatum: 
 * Erstellt von:
 */
using AnimeQuiz.Lib.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimeQuiz.Lib
{
    /// <summary>
    /// A Sample namespace
    /// </summary>
    public class CallbackObject
    {
        #region Felder
        private Guid _callbackID;
        #endregion

        #region Eigenschaften
        /// <summary>
        /// 
        /// </summary>
        public Guid CallbackID
        {
            get
            {
                return _callbackID;
            }

            private set
            {
                _callbackID = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Action<MessageBase> CallBack { get; private set; }
        #endregion

        #region Konstruktoren
        /// <summary>
        /// 
        /// </summary>
        public CallbackObject()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="callback"></param>
        public CallbackObject(Action<MessageBase> callback)
        {
            CallBack = callback;
            _callbackID = Guid.NewGuid();
        }
        #endregion

        #region Instanzmethoden
        #endregion

        #region Klassenmthoden
        #endregion
    }
}
