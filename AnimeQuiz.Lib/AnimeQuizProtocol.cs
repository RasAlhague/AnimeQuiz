﻿/* 
 * Erstelldatum: 
 * Erstellt von:
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimeQuiz.Lib
{
    /// <summary>
    /// A Sample namespace
    /// </summary>
    public enum AnimeQuizProtocol
    {
        Disconnect,
        ConnectionRequest,
        ConnectionResponse,
        LoginRequest,
        LoginReponse,
        LogOutRequest,
        PingRequest,
        PingReponse,
        SignUpRequest,
        SignUpResponse
    }
}
