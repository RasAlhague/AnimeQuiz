﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimeQuiz.Lib
{
    public static class AnimeQuizLogger
    {
        public static void WriteMessageToLog(string message, string path)
        {
            using (var writer = new StreamWriter(path, true))
            {
                writer.Write(message);
            }
        }

        public static void WriteExceptionToLog(Exception exception, string path)
        {
            Exception actualException = exception;
            StringBuilder errorMessage = new StringBuilder();

            while (actualException != null)
            {
                errorMessage.Append("Exception receiver " + DateTime.Now);
                errorMessage.AppendLine();
                errorMessage.Append(actualException);
                
                actualException = actualException.InnerException;
            }

            WriteMessageToLog(errorMessage.ToString(), path);
        }
    }
}
