/* 
 * Erstelldatum: 
 * Erstellt von: Florian Freyberger
 */
using AnimeQuiz.Lib.Encryption;
using System;

namespace AnimeQuiz.Lib.Users
{
    /// <summary>
    /// Base class for all users.
    /// </summary>
    public class User
    {
        #region Felder
        /// <summary>
        /// The username of the user. Must be unique when stored on the
        /// Server.
        /// </summary>
        protected string _username;
        /// <summary>
        /// The passwort of the user.
        /// </summary>
        protected string _passwort;
        #endregion

        #region Eigenschaften
        /// <summary>
        /// Gets the username.
        /// </summary>
        public virtual string Username
        {
            get
            {
                return _username;
            }

            protected set
            {
                _username = value;
            }
        }
        /// <summary>
        /// Gets the passwort
        /// </summary>
        public virtual string Passwort
        {
            get
            {
                return _passwort;
            }

            protected set
            {
                _passwort = value;
            }
        }

        public Guid ReceiverId { get; protected set; }
        #endregion

        #region Konstruktoren
        /// <summary>
        /// Initializes an empty user.
        /// </summary>
        public User()
        {
            _username = "";
            _passwort = "";
        }
        /// <summary>
        /// Initializes an User with given paramether. Doesnt encrypt passwords.
        /// </summary>
        /// <param name="username">The name of the new user</param>
        /// <param name="passwort">The password of the new user</param>
        public User(string username, string passwort)
        {
            _username = username;
            _passwort = passwort;
        }
        #endregion

        #region Instanzmethoden
        public void SetNewReceiverId(Guid newReceiverId)
        {
            ReceiverId = newReceiverId;
        }
        #endregion

        #region Klassenmthoden
        /// <summary>
        /// Creates an User with encrypted username and passwort.
        /// </summary>
        /// <param name="username">unencrypted username</param>
        /// <param name="password">Unencrypted passwort</param>
        /// <param name="encryptUsername"></param>
        /// <returns>User with encrypted passwort and with possible encrypted username</returns>
        /// <remarks>Returns null if creation failed.</remarks>
        public static User CreateSecureUser(string username, string password, bool encryptUsername = false, EncryptionLevel level = EncryptionLevel.Highest)
        {
            if(CheckPasswortSecurity(password, PasswordAdvisor.PasswordScore.Medium))
            {
                password = PasswordEncrypter.Encrypt(password, level);

                if (encryptUsername)
                    username = PasswordEncrypter.Encrypt(username, level);

                User newUser = new User(username, password);

                return newUser;
            }
            
            return null;
        }

        /// <summary>
        /// Checks if a Password is Secure.
        /// </summary>
        /// <param name="passwort">The password to check.</param>
        /// <param name="minScore">The min score the password should have to be accepted.</param>
        /// <returns>True if password score is greater or equal as the min score elsewise false</returns>
        public static bool CheckPasswortSecurity(string passwort, PasswordAdvisor.PasswordScore minScore)
        {
            if (PasswordAdvisor.CheckStrength(passwort) >= minScore)
                return true;
            else
                return false;
        }
        #endregion
    }
}
