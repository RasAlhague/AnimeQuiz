﻿using System;

namespace AnimeQuiz.Lib.Messages.Requests
{
    [Serializable]
    public class PingMessageRequest : MessageRequestBase
    {
        public DateTime PingTime { get; private set; }

        public PingMessageRequest() : base()
        {

        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        /// <param name="deleteCallBackAfterUse"></param>
        public PingMessageRequest(Guid sender, Guid receiver, DateTime time) : base(sender, receiver)
        {
            PingTime = time;
        }
    }
}