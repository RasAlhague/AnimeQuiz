﻿/*
 * Erstelldatum:
 * Erstellt von:
 */

using System;

namespace AnimeQuiz.Lib.Messages.Requests
{
    /// <summary>
    /// A Sample namespace
    /// </summary>
    [Serializable]
    public class ConnectionMessageRequest : MessageRequestBase
    {
        #region Properties

        #endregion 

        #region Konstruktoren

        /// <summary>
        ///
        /// </summary>
        public ConnectionMessageRequest() : base()
        {
        }

        #endregion Konstruktoren

        #region Methods
        #endregion
    }
}