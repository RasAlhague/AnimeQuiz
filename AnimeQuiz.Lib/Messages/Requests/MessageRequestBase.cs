﻿/* 
 * Erstelldatum: 
 * Erstellt von:
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimeQuiz.Lib.Messages.Requests
{
    /// <summary>
    /// A Sample namespace
    /// </summary>
    [Serializable]
    public abstract class MessageRequestBase : MessageBase
    {
        #region Felder
        #endregion

        #region Konstruktoren
        /// <summary>
        /// 
        /// </summary>
        protected MessageRequestBase() : this(Guid.Empty, Guid.Empty)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        /// <param name="deleteCallBackAfterUse"></param>
        protected MessageRequestBase(Guid sender, Guid receiver) : base(sender, receiver)
        {
        }
        #endregion

        #region Instanzmethoden
        #endregion

        #region Klassenmthoden
        #endregion
    }
}
