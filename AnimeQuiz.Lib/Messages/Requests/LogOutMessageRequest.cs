﻿/*
 * Erstelldatum:
 * Erstellt von:
 */

using AnimeQuiz.Lib.Users;
using System;

namespace AnimeQuiz.Lib.Messages.Requests
{
    /// <summary>
    /// A Sample namespace
    /// </summary>
    [Serializable]
    public class LogOutMessageRequest : MessageRequestBase
    {
        #region Felder

        private User _user;

        #endregion Felder

        #region Eigenschaften

        public User User
        {
            get
            {
                return this._user;
            }

            private set
            {
                _user = value;
            }
        }

        #endregion Eigenschaften

        #region Konstruktoren

        public LogOutMessageRequest() : base()
        {

        }

        public LogOutMessageRequest(Guid sender, Guid receiver, User user) : base(sender, receiver)
        {
            _user = user;
        }

        #endregion Konstruktoren

        #region Methods
        #endregion
    }
}