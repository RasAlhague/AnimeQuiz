﻿/*
 * Erstelldatum:
 * Erstellt von:
 */

using System;

namespace AnimeQuiz.Lib.Messages.Responses
{
    /// <summary>
    /// A Sample namespace
    /// </summary>
    [Serializable]
    public abstract class MessageResponseBase : MessageBase
    {
        #region Fields

        protected bool _deleteCallbackAfterUse;

        #endregion Fields

        #region Properties

        /// <summary>
        ///
        /// </summary>
        public virtual bool DeleteCallbackAfterUse
        {
            get { return _deleteCallbackAfterUse; }
            set { _deleteCallbackAfterUse = value; }
        }
        #endregion Properties

        #region Konstruktoren

        /// <summary>
        ///
        /// </summary>
        protected MessageResponseBase() : this(Guid.Empty, Guid.Empty, true)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        /// <param name="deleteCallBackAfterUse"></param>
        protected MessageResponseBase(Guid sender, Guid receiver) : this(sender, receiver, true)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        /// <param name="deleteCallBackAfterUse"></param>
        protected MessageResponseBase(Guid sender, Guid receiver, bool deleteCallBackAfterUse) : base(sender, receiver)
        {
            _deleteCallbackAfterUse = deleteCallBackAfterUse;
        }

        #endregion Konstruktoren
    }
}