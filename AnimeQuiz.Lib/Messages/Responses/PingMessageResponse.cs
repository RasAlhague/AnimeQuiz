﻿using System;

namespace AnimeQuiz.Lib.Messages.Responses
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    public class PingMessageResponse : MessageResponseBase
    {
        /// <summary>
        ///
        /// </summary>
        public DateTime PingTime { get; private set; }

        public PingMessageResponse() : base()
        {

        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        /// <param name="deleteCallBackAfterUse"></param>
        public PingMessageResponse(Guid sender, Guid receiver, DateTime time, bool deleteCallBackAfterUse = true) : base(sender, receiver, deleteCallBackAfterUse)
        {
            PingTime = time;
        }
    }
}