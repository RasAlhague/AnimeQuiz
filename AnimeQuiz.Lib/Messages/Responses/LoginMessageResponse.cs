﻿/*
 * Erstelldatum:
 * Erstellt von:
 */

using AnimeQuiz.Lib.Users;
using System;

namespace AnimeQuiz.Lib.Messages.Responses
{
    /// <summary>
    /// A Sample namespace
    /// </summary>
    [Serializable]
    public class LoginMessageResponse : MessageResponseBase
    {
        #region Felder

        private User _user;

        #endregion Felder

        #region Eigenschaften

        /// <summary>
        ///
        /// </summary>
        public User User
        {
            get { return _user; }

            private set { _user = value; }
        }

        #endregion Eigenschaften

        #region Konstruktoren

        public LoginMessageResponse() : base()
        {

        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        /// <param name="user"></param>
        public LoginMessageResponse(Guid sender, Guid receiver, User user) : this(sender, receiver, user, true)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        /// <param name="user"></param>
        /// <param name="deleteCallbackAfterUse"></param>
        public LoginMessageResponse(Guid sender, Guid receiver, User user, bool deleteCallbackAfterUse) : base(sender, receiver, deleteCallbackAfterUse)
        {
            _user = user;
        }

        #endregion Konstruktoren

        #region Methods
        #endregion
    }
}