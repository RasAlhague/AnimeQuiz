﻿/*
 * Erstelldatum:
 * Erstellt von:
 */

using System;

namespace AnimeQuiz.Lib.Messages.Responses
{
    /// <summary>
    /// A Sample namespace
    /// </summary>
    [Serializable]
    public class ConnectionMessageResponse : MessageResponseBase
    {
        #region Properties

        #endregion

        #region Konstruktoren
        public ConnectionMessageResponse() : base()
        {

        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        /// <param name="deleteCallBackAfterUse"></param>
        public ConnectionMessageResponse(Guid sender, Guid receiver) : this(sender, receiver, true)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        /// <param name="deleteCallBackAfterUse"></param>
        public ConnectionMessageResponse(Guid sender, Guid receiver, bool deleteCallBackAfterUse) : base(sender, receiver, deleteCallBackAfterUse)
        {
        }

        #endregion Konstruktoren

        #region Methods
        #endregion
    }
}