﻿/*
 * Erstelldatum:
 * Erstellt von:
 */

using AnimeQuiz.Lib;
using AnimeQuiz.Lib.Messages;
using AnimeQuiz.Lib.Messages.Responses;
using AnimeQuiz.Lib.Users;
using System;

namespace AnimeQuiz.Lib.Messages.Responses
{
    /// <summary>
    /// A Sample namespace
    /// </summary>
    [Serializable]
    public class SignUpMessageResponse : MessageResponseBase
    {
        #region Felder

        private User _user;

        #endregion Felder

        #region Eigenschaften

        /// <summary>
        ///
        /// </summary>
        public User User
        {
            get
            {
                return _user;
            }

            private set
            {
                _user = value;
            }
        }

        #endregion Eigenschaften

        #region Konstruktoren

        public SignUpMessageResponse() : base()
        {

        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        /// <param name="user"></param>
        public SignUpMessageResponse(Guid sender, Guid receiver, User user) : this(sender, receiver, user, true)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        /// <param name="user"></param>
        /// <param name="deleteCallbackAfterUse"></param>
        public SignUpMessageResponse(Guid sender, Guid receiver, User user, bool deleteCallbackAfterUse) : base(sender, receiver, deleteCallbackAfterUse)
        {
            _user = user;
        }

        #endregion Konstruktoren

        #region Methods
        #endregion
    }
}