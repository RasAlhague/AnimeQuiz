﻿/*
 * Erstelldatum:
 * Erstellt von:
 */

using AnimeQuiz.Lib.Messages.Requests;
using AnimeQuiz.Lib.Messages.Responses;
using System;

namespace AnimeQuiz.Lib.Messages
{
    /// <summary>
    /// A Sample namespace
    /// </summary>
    [Serializable]
    public abstract class MessageBase
    {
        #region Felder

        protected Guid _sender;
        protected Guid _receiver;
        protected Guid _callbackID;

        #endregion Felder

        #region Eigenschaften

        /// <summary>
        ///
        /// </summary>
        public virtual Guid Sender
        {
            get
            {
                return _sender;
            }

            protected set
            {
                _sender = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public virtual Guid Receiver
        {
            get
            {
                return _receiver;
            }

            protected set
            {
                _receiver = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public virtual Guid CallbackID
        {
            get
            {
                return _callbackID;
            }

            set
            {
                _callbackID = value;
            }
        }

    /// <summary>
    ///
    /// </summary>
    public virtual Exception Exception { get; set; }

        #endregion Eigenschaften

        #region Konstruktoren

        /// <summary>
        ///
        /// </summary>
        protected MessageBase() : this(Guid.Empty, Guid.Empty)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        protected MessageBase(Guid sender, Guid receiver)
        {
            this._sender = sender;
            this._receiver = receiver;
        }

        #endregion Konstruktoren

        #region Methods
        #endregion

        #region Class-Methods
       
        #endregion
    }
}