﻿/*
 * Erstelldatum:
 * Erstellt von:
 */

using System;

namespace AnimeQuiz.Lib.Messages
{
    /// <summary>
    /// A Sample namespace
    /// </summary>
    [Serializable]
    public class DisconnectMessage : MessageBase
    {
        #region Properties

        #endregion
        
        #region Konstruktoren

        public DisconnectMessage() : base()
        {

        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        /// <param name="deleteCallBackAfterUse"></param>
        public DisconnectMessage(Guid sender, Guid receiver) : base(sender, receiver)
        {
        }


        #endregion Konstruktoren

        #region Methods
        #endregion
    }
}