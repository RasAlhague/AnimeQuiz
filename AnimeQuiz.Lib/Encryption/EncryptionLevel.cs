﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimeQuiz.Lib.Encryption
{
    /// <summary>
    /// Level of encryption. Is used to determine which type of encryption provider should be used.
    /// </summary>
    public enum EncryptionLevel
    {
        /// <summary>
        /// Uses MD5
        /// </summary>
        None,
        Low,
        Middel,
        High,
        Highest
    }
}
