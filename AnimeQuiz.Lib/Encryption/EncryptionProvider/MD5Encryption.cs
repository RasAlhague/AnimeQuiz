﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimeQuiz.Lib.Encryption.EncryptionProvider
{
    /// <summary>
    /// Encrypts strings using MD5 Hashing
    /// </summary>
    public class MD5EncryptionProvider : IEncryption
    {
        /// <summary>
        /// Always throwing an Exception because MD5 is only 1 way
        /// </summary>
        /// <param name="encryptedString"></param>
        /// <returns></returns>
        public string Decryption(string encryptedString)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="unencryptedString"></param>
        /// <returns></returns>
        public string Encrypt(string unencryptedString)
        {
            // TODO: Implement MD5 Hashing algorithm
            throw new NotImplementedException();
        }
    }
}
