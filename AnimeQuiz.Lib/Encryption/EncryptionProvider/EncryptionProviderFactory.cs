﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimeQuiz.Lib.Encryption.EncryptionProvider
{
    /// <summary>
    /// Creates encryption Provider.
    /// </summary>
    public static class EncryptionProviderFactory
    {
        /// <summary>
        /// Creates an new Concret object of IEncryption.
        /// </summary>
        /// <param name="level">Determines the Encryption Provider</param>
        /// <returns>Currently dummy Provider</returns>
        public static IEncryption CreateEncryptionProvider(EncryptionLevel level)
        {
            IEncryption encryptionProvider = null;

            // TODO: Implement the correct Providers.
            switch (level)
            {
                case EncryptionLevel.None:
                    encryptionProvider = new MD5EncryptionProvider();
                    break;
                case EncryptionLevel.Low:
                    encryptionProvider = new MD5EncryptionProvider();
                    break;
                case EncryptionLevel.Middel:
                    encryptionProvider = new MD5EncryptionProvider();
                    break;
                case EncryptionLevel.High:
                    encryptionProvider = new MD5EncryptionProvider();
                    break;
                case EncryptionLevel.Highest:
                    encryptionProvider = new MD5EncryptionProvider();
                    break;
                default:
                    encryptionProvider = null;
                    break;
            }

            return encryptionProvider;
        }
    }
}
