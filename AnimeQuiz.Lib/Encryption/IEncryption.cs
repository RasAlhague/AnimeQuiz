﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimeQuiz.Lib.Encryption
{
    /// <summary>
    /// Implements encryption methods.
    /// </summary>
    public interface IEncryption
    {
        /// <summary>
        /// Encrypts a string.
        /// </summary>
        /// <param name="unencryptedString">String to encrypt</param>
        /// <returns>Encrypted string</returns>
        string Encrypt(string unencryptedString);
        /// <summary>
        /// Decrypts a string.
        /// </summary>
        /// <param name="encryptedString">string to decrypt</param>
        /// <returns>decrypted string</returns>
        string Decryption(string encryptedString);
    }
}
