﻿using AnimeQuiz.Lib.Messages;
using AnimeQuiz.Server.Lib;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AnimeQuiz.Server.Console
{
    public static class Program
    {
        private static AnimeQuizServer server = null;

        static void Main(string[] args)
        {
            bool noException = true;
            int port = 0;
            string serverName;
            server = null;

            do
            {
                try
                {
                    System.Console.WriteLine("Anime Quiz Server: Ver. 01.00");
                    System.Console.WriteLine("---------------------------\n");

                    System.Console.WriteLine("Enter Port: ");
                    System.Console.Write(">>");
                    port = Convert.ToInt32(System.Console.ReadLine());
                    System.Console.WriteLine("\nEnter Server Name: ");
                    System.Console.Write(">>");
                    serverName = System.Console.ReadLine();

                    server = new AnimeQuizServer(port, serverName);

                    server.StartServer();
                    server.ClientConnected += NewClientConnected;
                    server.ClientDisconnected += ClientDisconnect;

                    Task.Run(() => server.StartAcceptingClients());

                    while (true)
                    {
                        //TODO: Menü einbauen
                        //Console.WriteLine("");


                        System.Console.WriteLine("Do you want to shutdown the server? (y/n)");
                        System.Console.Write(">>");

                        string input = System.Console.ReadLine();

                        if (input == "y" || input == "Y")
                        {
                            noException = true;
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Console.Clear();
                    System.Console.Write(ex);

                    System.Console.WriteLine("\n\nAn error ocurred!");
                    System.Console.WriteLine("\nDo you want to restart the Server? (y/n)");
                    System.Console.Write(">>");

                    string input = System.Console.ReadLine();

                    if (input == "y" || input == "Y")
                        noException = false;
                    else
                        noException = true;
                    System.Console.Clear();
                }
            } while (!noException);
        }

        public static void DisplayMessageReceived(MessageBase message)
        {
            if (message != null)
            {
                IPEndPoint senderEndPoint;
                IPEndPoint receiverEndPoint;

                if (message.Receiver != server.ServerID)
                {
                    Receiver sender = server.ActivReceivers.FirstOrDefault((r) => r.ReceiverID == message.Sender);
                    Receiver receiver = server.ActivReceivers.FirstOrDefault((r) => r.ReceiverID == message.Receiver);

                    senderEndPoint = sender?.AssociatedClient.Client.RemoteEndPoint as IPEndPoint;
                    receiverEndPoint = receiver?.AssociatedClient.Client.RemoteEndPoint as IPEndPoint;
                }
                else
                {
                    Receiver sender = server.ActivReceivers.FirstOrDefault((r) => r.ReceiverID == message.Sender);

                    senderEndPoint = (IPEndPoint)sender?.AssociatedClient.Client.RemoteEndPoint;
                    receiverEndPoint = (IPEndPoint)server?.Listener.LocalEndpoint;
                }



                if (senderEndPoint == null)
                    senderEndPoint = new IPEndPoint(IPAddress.Parse("0.0.0.0"), 0);
                if (receiverEndPoint == null)
                    receiverEndPoint = new IPEndPoint(IPAddress.Parse("0.0.0.0"), 0);

                System.Console.WriteLine($"{DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.fff")} --> Message Received, of type: {message.GetType().ToString()}");
                System.Console.WriteLine($"\tfrom <{senderEndPoint.Address}::{senderEndPoint.Port}> to <{receiverEndPoint.Address}::{receiverEndPoint.Port}>\n");
            }
            else
            {
                System.Console.WriteLine($"{DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.fff")} --> Unreadable Message Received");
            }
        }

        public static void NewClientConnected(Guid newReceiverID)
        {
            Receiver newClient = server.ActivReceivers.FirstOrDefault((r) => r.ReceiverID == newReceiverID);

            IPEndPoint endPoint = (IPEndPoint)newClient.AssociatedClient.Client.RemoteEndPoint;
            string connection = endPoint.Address + ":" + endPoint.Port;

            System.Console.WriteLine($"{DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.fff")} --> New Client connected on <{connection}>");
            newClient.MessageReceived += DisplayMessageReceived;
        }

        public static void ClientDisconnect(IPEndPoint clientEndpoint)
        {
            System.Console.WriteLine($"{DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.fff")} --> Client <{clientEndpoint.Address}{clientEndpoint.Port}> Disconnected.");
        }

        //public static void 
    }
}
