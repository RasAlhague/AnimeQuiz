using AnimeQuiz.Lib;
using AnimeQuiz.Lib.Delegates;
using AnimeQuiz.Lib.Messages;
using AnimeQuiz.Lib.Messages.Requests;
using AnimeQuiz.Lib.Messages.Responses;
using AnimeQuiz.Lib.Users;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace AnimeQuiz.Server.Lib
{
    /// <summary>
    /// Receives the actual messages and communicates with the Server.
    /// This is the endpoint that every client uses after connecting to the Server.
    /// </summary>
    public class Receiver
    {
        #region "Felder"
        /// <summary>
        /// Guid of the receiver. Is used to identify an Receiver.
        /// Should never exist two times on the same server.
        /// </summary>
        private Guid _receiverID;
        /// <summary>
        /// Contains the tcpClient which is connected to the client.
        /// </summary>
        private TcpClient _associatedClient;
        /// <summary>
        /// Contains the server which is hosting the receiver.
        /// </summary>
        private AnimeQuizServer _parentServer;
        /// <summary>
        /// Contains the state of the receiver.
        /// </summary>
        private ReceiverState _state;
        /// <summary>
        /// Contains the User which is associated with the receiver.
        /// </summary>
        private User _user;

        /// <summary>
        /// The Queue which contains the messages which need to be processesd and sended to the Clients.
        /// </summary>
        private ConcurrentQueue<MessageBase> messageQueue;
        /// <summary>
        /// List of Callbacks which are associated to some messages.
        /// </summary>
        private List<CallbackObject> callbacks;
        /// <summary>
        /// The thread on which the receiver receives messages.
        /// </summary>
        private Thread receivingThread;
        /// <summary>
        /// The thread on which the receiver sends messages.
        /// </summary>
        private Thread sendingThread;
        #endregion

        #region "Events"
        public event MessageReceivedHandler MessageReceived;
        public event InvalidMessageReceivedHandler InvalidMessageReceived;
        public event DisconnectedHandler Disconnected;
        #endregion

        #region "Eigenschaften"
        /// <summary>
        /// Gets the Unique Id of the receiver.
        /// </summary>
        public Guid ReceiverID
        {
            get
            {
                return _receiverID;
            }
            private set
            {
                _receiverID = value;
            }
        }
        /// <summary>
        /// Gets the AssociatedClient of type <see cref="TcpClient"/>. 
        /// </summary>
        public TcpClient AssociatedClient
        {
            get
            {
                return _associatedClient;
            }
            private set
            {
                _associatedClient = value;
            }
        }
        /// <summary>
        /// Gets the <see cref="AnimeQuizServer"/> which this Receiver was created on.
        /// </summary>
        public AnimeQuizServer ParentServer
        {
            get
            {
                return _parentServer;
            }
            private set
            {
                _parentServer = value;
            }
        }
        /// <summary>
        /// Gets the Current <seealso cref="ReceiverState"/> of the Receiver 
        /// </summary>
        public ReceiverState State
        {
            get
            {
                return _state;
            }

            private set
            {
                _state = value;
            }
        }
        /// <summary>
        /// Gets or sets the user which is associated with the receiver.
        /// </summary>
        public User User
        {
            get
            {
                return _user;
            }

            set
            {
                _user = value;
            }
        }
        #endregion

        #region "Konstruktoren"        
        /// <summary>
        /// Standard contruct.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="parent"></param>
        public Receiver(TcpClient client, AnimeQuizServer parent)
        {
            if(client == null)
                throw new ArgumentNullException("client", "client is null");
            if(parent == null)
                throw new ArgumentNullException("parent", "parent is null");
            _associatedClient = client;
            _parentServer = parent;

            if (_associatedClient.Connected)
                _state = ReceiverState.Connected;
            else
                _state = ReceiverState.Disconnected;

            _receiverID = Guid.NewGuid();
        }
        #endregion

        #region "Instanzmethoden"
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReceiverState StartReceiver()
        {
            if (_associatedClient.Connected)
            {
                messageQueue = new ConcurrentQueue<MessageBase>();
                callbacks = new List<CallbackObject>();

                State = ReceiverState.Started;

                receivingThread = new Thread(ReadingLoop);
                receivingThread.IsBackground = true;
                receivingThread.Start();

                sendingThread = new Thread(SendingLoop);
                sendingThread.IsBackground = true;
                sendingThread.Start();
            }

            return State;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Disconnect()
        {
            messageQueue.Clear();
            callbacks.Clear();
            // TODO: Vorher eine Nachricht zum disconnect senden.
            AssociatedClient.Close();
            State = ReceiverState.Disconnected;
            Disconnected?.Invoke(ReceiverID);
        }

        /// <summary>
        /// 
        /// </summary>
        public void ErrorDisconnect()
        {
            messageQueue.Clear();
            callbacks.Clear();

            DisconnectMessage disconnectMessage = new DisconnectMessage(_parentServer.ServerID, ReceiverID);
            SendMessageToQueue(disconnectMessage);

            Thread.Sleep(1000);

            AssociatedClient.Close();
            State = ReceiverState.Disconnected;
            Disconnected?.Invoke(ReceiverID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Guid SetNewReceiverID()
        {
            ReceiverID = Guid.NewGuid();

            return ReceiverID;
        }

        /// <summary>
        /// 
        /// </summary>
        private void ReadingLoop()
        {
            while (State == ReceiverState.Started)
            {
                try
                {
                    if (AssociatedClient.Available > 0)
                    {
                        BinaryFormatter formater = new BinaryFormatter();
                        //TODO: Decrypt Message before processing
                        var deserializedMessage = formater.Deserialize(AssociatedClient.GetStream());

                        MessageBase message = deserializedMessage as MessageBase;

                        if (message != null)
                        {
                            try
                            {
                                MessageReceived?.Invoke(message);
                                ProcessMessage(message);
                            }
                            catch (InvalidOperationException ex)
                            {
                                ErrorDisconnect();
                                AnimeQuizLogger.WriteExceptionToLog(ex, "C:\\ProgrammTestLogs\\ReceiverLog.txt");
                                Disconnect();
                            }
                            catch (Exception ex) // TODO: SendMessageToQueue(new DisconnectMessage(this.ReceiverID, this.)); An eine neue exception binden. und dann Disconnect();
                            {
                                AnimeQuizLogger.WriteExceptionToLog(ex, "C:\\ProgrammTestLogs\\ReceiverLog.txt");
                                Disconnect();
                            }
                        }
                    }
					else
					{
						Thread.Sleep(1);
					}
                }
                catch(Exception ex)
                {
                    AnimeQuizLogger.WriteExceptionToLog(ex, "C:\\ProgrammTestLogs\\ReceiverLog.txt");
                    Disconnect();
                }
                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void SendingLoop()
        {
            while(State == ReceiverState.Started)
            {

                if (messageQueue.Count > 0)
                {
                    BinaryFormatter formatter = new BinaryFormatter();

                    try
                    {
						//TODO: Encrypt Message before Serialization
						if (messageQueue.TryDequeue(out MessageBase message))
						{
							formatter.Serialize(AssociatedClient.GetStream(), message);
						}
                    }
                    catch (Exception ex)// TODO: SendMessageToQueue(new DisconnectMessage(this.ReceiverID, this.)); An eine neue exception binden. und dann Disconnect();
                    {
                        AnimeQuizLogger.WriteExceptionToLog(ex, "C:\\ProgrammTestLogs\\ReceiverLog.txt");
                        Disconnect();
                    }
                }
				else
				{
					Thread.Sleep(1);
				}
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void SendMessageToQueue(MessageBase message)
        {
            message.CallbackID = Guid.Empty;

            messageQueue.Enqueue(message);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="callback"></param>
        public void SendMessageToQueue(MessageBase message, Action<MessageBase> callback)
        {
            CallbackObject callbackObject = new CallbackObject(callback);
            callbacks.Add(callbackObject);

            message.CallbackID = callbackObject.CallbackID;
            messageQueue.Enqueue(message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response"></param>
        public void SendResponseToQueue(MessageBase response)
        {
            messageQueue.Enqueue(response);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        private void ProcessMessage(MessageBase message)
        {
            if (message is MessageRequestBase)
            {
                MessageRequestBase messageRequest = message as MessageRequestBase;
                ProcessMessageRequest(messageRequest);
            }
            else if (message is MessageResponseBase)
            {
                MessageResponseBase messageResponse = message as MessageResponseBase;
                ProcessMessageResponse(messageResponse);
            }
            else if (message is DisconnectMessage)
            {
                DisconnectMessage messageResponse = message as DisconnectMessage;
                Disconnect();
            }
            else
            {
                InvalidMessageReceived?.Invoke(ReceiverID);
                throw new ArgumentException("Message is not a valid Typ");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        private void ProcessMessageResponse(MessageResponseBase messageResponse)
        {
            if (messageResponse is PingMessageResponse)
            {
                CallCorrespondingCallback(messageResponse);
            }
            else
            {
                InvalidMessageReceived(ReceiverID);
                throw new ArgumentException("Message is not a valid Typ");
            }
        }

        private void CallCorrespondingCallback(MessageResponseBase messageResponse)
        {
            var correspondingCallback = callbacks.FirstOrDefault((x) => x.CallbackID == messageResponse.CallbackID);

            if (correspondingCallback != null)
            {
                correspondingCallback.CallBack.Invoke(messageResponse);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageRequest"></param>
        private void ProcessMessageRequest(MessageRequestBase messageRequest)
        {
            if (messageRequest is LoginMessageRequest)
            {

            }
            else if (messageRequest is ConnectionMessageRequest)
            {
                var connectionRequest = messageRequest as ConnectionMessageRequest;

                //TODO: Bevor really sending an answer do blacklist checking
                ConnectionMessageResponse connectionResponse = new ConnectionMessageResponse(ParentServer.ServerID, ReceiverID);
                connectionResponse.CallbackID = connectionRequest.CallbackID;

                SendResponseToQueue(connectionResponse);
            }
            else if (messageRequest is LogOutMessageRequest)
            {
                
            }
            else if (messageRequest is PingMessageRequest)
            {
                var pingRequest = messageRequest as PingMessageRequest;

                PingMessageResponse pingResponse = new PingMessageResponse(ParentServer.ServerID, ReceiverID, pingRequest.PingTime, false);
                pingResponse.CallbackID = pingRequest.CallbackID;

                SendResponseToQueue(pingResponse);
            }
            else if (messageRequest is SignUpMessageRequest)
            {

            }
            else
            {
                InvalidMessageReceived?.Invoke(ReceiverID);
                throw new ArgumentException("Message is not a valid Typ");
            }
        }
        #endregion

        #region "RequestHandler"
        #endregion

        #region "Klassenmethoden"
        #endregion
    }
}
