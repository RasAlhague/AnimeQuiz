/* 
 * Erstelldatum: 
 * Erstellt von:
 */
using AnimeQuiz.Lib.Users;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace AnimeQuiz.Server.Lib
{
    public class AnimeQuizServer
    { 
        #region "Felder"
        private TcpListener _listener;
        private Guid _serverID;
        private int _port;
        private ServerState _state = ServerState.Down;
        private string _serverName;
        #endregion

        #region "Events"
        public event ClientConnectedHandler ClientConnected;

        public event ClientDisconnectedHandler ClientDisconnected;
        #endregion

        #region "Eigenschaften"
        /// <summary>
        /// 
        /// </summary>
        public int Port
        {
            get
            {
                return _port;
            }
            private set
            {
                _port = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ServerState State
        {
            get
            {
                return _state;
            }
            private set
            {
                _state = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public TcpListener Listener
        {
            get
            {
                return _listener;
            }
            private set
            {
                _listener = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Guid ServerID
        {
            get
            {
                return _serverID;
            }
            private set
            {
                _serverID = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ServerName
        {
            get
            {
                return _serverName;
            }
            set
            {
                _serverName = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public List<Receiver> ActivReceivers { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public List<User> RegistratedUsers { get; private set; }
        #endregion  

        #region "Konstruktoren"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="port"></param>
        public AnimeQuizServer(int port) : this(port, "DefaultTesting01")
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="port"></param>
        public AnimeQuizServer(int port, string name)
        {
            _serverName = name;
            _port = port;

            RegistratedUsers = new List<User>();
            ActivReceivers = new List<Receiver>();

            _serverID = Guid.NewGuid();

            _listener = new TcpListener(IPAddress.Any, Port);
        }
        #endregion

        #region "Instanzmethoden"
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool StartServer()
        {
            try
            {
                _listener.Start();
                State = ServerState.Blocked;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false; 
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public void StartAcceptingClients()
        {
            while (State != ServerState.ShutingDown)
            {
                try
                {
                    Receiver newReceiver = new Receiver(Listener.AcceptTcpClient(), this);
                    
                    ActivReceivers.Add(newReceiver);
                    ClientConnected?.Invoke(newReceiver.ReceiverID);

                    Task.Run(() => newReceiver.StartReceiver());
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disconnectedPartnerID"></param>
        public void DisconnectReceiver(Guid disconnectedPartnerID)
        {
            Receiver receiverToRemove = ActivReceivers.FirstOrDefault((r) => r.ReceiverID == disconnectedPartnerID);

            if(receiverToRemove != null)
            {
                IPEndPoint receiverEndpoint = (IPEndPoint)receiverToRemove.AssociatedClient.Client.RemoteEndPoint;

                IPEndPoint clientEndpoint = new IPEndPoint(IPAddress.Parse(receiverEndpoint.Address.ToString()), receiverEndpoint.Port);

                ClientDisconnected?.Invoke(clientEndpoint);
            }
            ActivReceivers.Remove(receiverToRemove);
        }

        #region "Klassenmethoden"
        #endregion
    }
}
