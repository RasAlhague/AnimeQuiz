﻿namespace AnimeQuiz.Server.Lib
{
    public enum ServerState
    {
        Down,
        Listening,
        Blocked,
        ShutingDown
    }
}
