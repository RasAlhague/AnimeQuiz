﻿using System;
using Avalonia;
using Avalonia.Logging.Serilog;
using AnimeQuiz.Server.UI.ViewModels;
using AnimeQuiz.Server.UI.Views;

namespace AnimeQuiz.Server.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            BuildAvaloniaApp().Start<MainWindow>(() => new MainWindowViewModel());
        }

        public static AppBuilder BuildAvaloniaApp()
            => AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .UseReactiveUI()
                .LogToDebug();
    }
}
