using Avalonia;
using Avalonia.Markup.Xaml;

namespace AnimeQuiz.Server.UI
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
   }
}