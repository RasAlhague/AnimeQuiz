﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimeQuiz.Client.Lib
{
    public enum ClientState
    {
        Down,
        Connected,
        Started,
        Stopped,
        Disconnected
    }
}
