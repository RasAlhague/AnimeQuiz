/* 
 * Erstelldatum: 
 * Erstellt von:
 */
using AnimeQuiz.Lib;
using AnimeQuiz.Lib.Delegates;
using AnimeQuiz.Lib.Messages;
using AnimeQuiz.Lib.Messages.Requests;
using AnimeQuiz.Lib.Messages.Responses;
using AnimeQuiz.Lib.Users;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace AnimeQuiz.Client.Lib
{
    /// <summary>
    /// A Sample namespace
    /// </summary>
    public class AnimeQuizClient
    {

		#region "Felder"
		private Thread receivingThread;
        private Thread sendingThread;
        private List<CallbackObject> callbacks;
        #endregion

        #region "Events"
        /// <summary>
        /// 
        /// </summary>
        public event InvalidMessageReceivedHandler InvalidMessageReceived;

        public event DisconnectedHandler Disconnected;

        public event MessageReceivedHandler MessageReceived;
        #endregion

        #region "Eigenschaften"
        /// <summary>
        /// 
        /// </summary>
        public ConcurrentQueue<MessageBase> MessageQueue { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public TcpClient Client { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public ClientState State { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public User User { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public Guid ClientID { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public IPEndPoint ClientEndPoint { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public IPEndPoint ServerEndPoint { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public Guid ServerID { get; set; }
        #endregion
    
        #region "Konstruktor"
        /// <summary>
        /// 
        /// </summary>
        private AnimeQuizClient()
        {
            State = ClientState.Down;
            User = null;
            MessageQueue = new ConcurrentQueue<MessageBase>();
            callbacks = new List<CallbackObject>();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientEndpoint"></param>
        /// <param name="serverEndpoint"></param>
        public AnimeQuizClient(IPEndPoint serverEndpoint)
        {
            State = ClientState.Down;
            User = null;
            MessageQueue = new ConcurrentQueue<MessageBase>();
            callbacks = new List<CallbackObject>();
            ServerEndPoint = serverEndpoint;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientIp"></param>
        /// <param name="clientPort"></param>
        /// <param name="serverIp"></param>
        /// <param name="ServerPort"></param>
        public AnimeQuizClient(IPAddress serverIp, int ServerPort)
        {
            State = ClientState.Down;
            User = null;
            MessageQueue = new ConcurrentQueue<MessageBase>();
            callbacks = new List<CallbackObject>();
            ServerEndPoint = new IPEndPoint(serverIp, ServerPort);
        }
        #endregion

        #region "Instanzmethoden"
        /// <summary>
        /// 
        /// </summary>
        public void ConnectToServer()
        {
            try
            {
                Client = new TcpClient();
                Client.Connect(ServerEndPoint);
                ClientEndPoint = (IPEndPoint)Client.Client.LocalEndPoint;
            }
            catch (ArgumentNullException e)
            {
                AnimeQuizLogger.WriteExceptionToLog(e, "C:\\ProgrammTestLogs\\ClientLog.txt");
                Debug.WriteLine(e.Message);
            }
            catch (SocketException e)
            {
                AnimeQuizLogger.WriteExceptionToLog(e, "C:\\ProgrammTestLogs\\ClientLog.txt");
                Debug.WriteLine(e.Message);
            }
            catch (ObjectDisposedException e)
            {
                AnimeQuizLogger.WriteExceptionToLog(e, "C:\\ProgrammTestLogs\\ClientLog.txt");
                Debug.WriteLine(e.Message);
            }
            finally
            {
                if (Client.Connected)
                {
                    State = ClientState.Connected;
                }
                else
                {
                    State = ClientState.Disconnected;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverEndpoint"></param>
        public void ConnectToServer(IPEndPoint serverEndpoint)
        {
            ServerEndPoint = serverEndpoint;

            ConnectToServer();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverIp"></param>
        /// <param name="serverPort"></param>
        public void ConnectToServer(IPAddress serverIp, int serverPort)
        {
            ServerEndPoint = new IPEndPoint(serverIp, serverPort);

            ConnectToServer();
        }
        /// <summary>
        /// 
        /// </summary>
        public void Disconnect()
        {
            MessageQueue.Clear();
            callbacks.Clear();
            // TODO: Vorher eine Nachricht zum disconnect senden.
            Client.Close();
            State = ClientState.Disconnected;
            Disconnected?.Invoke(ServerID);
        }
        /// <summary>
        /// 
        /// </summary>
        public void StartRunning()
        {
            State = ClientState.Started;

            receivingThread = new Thread(ReadingLoop);
            receivingThread.IsBackground = true;
            receivingThread.Start();

            sendingThread = new Thread(SenderLoop);
            sendingThread.IsBackground = true;
            sendingThread.Start();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void SendMessageToQueue(MessageBase message)
        {
            message.CallbackID = Guid.Empty;

            MessageQueue.Enqueue(message);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="callback"></param>
        public void SendMessageToQueue(MessageBase message, Action<MessageBase> callback)
        {
            CallbackObject callbackObject = new CallbackObject(callback);
            callbacks.Add(callbackObject);

            message.CallbackID = callbackObject.CallbackID;
            MessageQueue.Enqueue(message);   
        }
        /// <summary>
        /// 
        /// </summary>
        private void SenderLoop()
        {
            while (State == ClientState.Started)
            {
                if (MessageQueue.Count > 0)
                {
                    BinaryFormatter formatter = new BinaryFormatter();

                    try
                    {
						//TODO Encrypt before sending
						if (MessageQueue.TryDequeue(out MessageBase message))
						{
							formatter.Serialize(Client.GetStream(), message);
						}
                    }
                    catch (Exception ex)// TODO: SendMessageToQueue(new DisconnectMessage(this.ReceiverID, this.)); An eine neue exception binden. und dann Disconnect();
                    {
                        Disconnect();
                        AnimeQuizLogger.WriteExceptionToLog(ex, "C:\\ProgrammTestLogs\\ClientLog.txt");
                    }
                }
                else
                {
                    Thread.Sleep(1);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ReadingLoop()
        {
            while (State == ClientState.Started)
            { 
                //TODO: Add Try Catch
                if(Client?.Available > 0)
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    var deserializedMessage = formatter.Deserialize(Client.GetStream());

                    MessageBase message = deserializedMessage as MessageBase;


                    if (message != null)
                    {
                        //TODO Decrypt before processing
                        ProcessMessage(message);
                    }
                    else
                    {
                        InvalidMessageReceived?.Invoke(ServerID);
                    }
                }
                else
                {
                    Thread.Sleep(1);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        private void ProcessMessage(MessageBase message)
        {
            MessageReceived?.Invoke(message);


            if (message is MessageRequestBase)
            {
                MessageRequestBase request = message as MessageRequestBase;
                ProcessRequest(request);
            }
            else if(message is MessageResponseBase)
            {
                MessageResponseBase response = message as MessageResponseBase;
                ProcessResponse(response);
            }
            else if (message is DisconnectMessage)
            {
                DisconnectMessage disconnectMessage = message as DisconnectMessage;

                Disconnect();
            }
            else
            {
                InvalidMessageReceived?.Invoke(message.Sender);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        private void ProcessResponse(MessageResponseBase responseMessage)
        {
            Type type = responseMessage.GetType();

            if (responseMessage is LoginMessageResponse)
            {
                CallCorrespondingCallback(responseMessage);
            }
            else if (responseMessage is PingMessageResponse)
            {
                CallCorrespondingCallback(responseMessage);
            }
            else if (responseMessage is ConnectionMessageResponse)
            {
                if (responseMessage.Sender != Guid.Empty)
                    ClientID = responseMessage.Receiver;
                CallCorrespondingCallback(responseMessage);
            }
            else
            {
                InvalidMessageReceived?.Invoke(responseMessage.Sender);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        private void ProcessRequest(MessageRequestBase requestMessage)
        {
            Type type = requestMessage.GetType();

            if (type is LoginMessageRequest)
            {
                if(Debugger.IsAttached)
                    Debugger.Break();
            }
            else
            {
                InvalidMessageReceived?.Invoke(requestMessage.Sender);
            }
        }

        private void CallCorrespondingCallback(MessageResponseBase messageResponse)
        {
            try
            {
                var correspondingCallback = callbacks.FirstOrDefault((x) => x.CallbackID == messageResponse.CallbackID);

                if (correspondingCallback != null)
                {
                    correspondingCallback.CallBack.Invoke(messageResponse);
                }
            }
            catch (Exception ex)
            {
                AnimeQuizLogger.WriteExceptionToLog(ex, "C:\\ProgrammTestLogs\\ClientLog.txt");
            }          
        }
        #endregion

        #region "Klassenmethoden"
        #endregion

        #region "EventHandler"

        #endregion "EventHandler"
    }
}
